include ${FSLCONFDIR}/default.mk

PROJNAME = xtract

SCRIPTS = xtract xtract_viewer xtract_stats xtract_blueprint xtract_qc create_blueprint.py report_xtract_qc.py get_wgb
